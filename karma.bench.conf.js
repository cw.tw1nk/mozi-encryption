module.exports = function(karma) {
  const config = {
    autoWatch: false,
    basePath: '',
    // for full list of options, see
    // https://github.com/FormidableLabs/karma-benchmarkjs-reporter
    benchmarkReporter: {
      decorator: '-',
      terminalWidth: 60,
      hzWidth: 4,
      hzUnits: 'ops/sec',
      browserWidth: 40,
      showBrowser: false,
      showSuiteSummary: true
    },
    browsers: ['Chrome'],
    colors: true,
    concurrency: 1,
    exclude: [],
    files: ['src/**/*.bench.js'],
    frameworks: ['benchmark'],
    junitReporter: {
      outputDir: 'reports',
      outputFile: 'benchmark.xml'
    },
    logLevel: karma.LOG_INFO,
    port: 9876,
    preprocessors: {},
    reporters: ['benchmark'],
    singleRun: true
  };
  karma.set(config);
};
