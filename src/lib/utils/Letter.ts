
export class Letter {
	private readonly _letter: string;
	private readonly _frequency: number;

	constructor(letter: string, frequency: number) {
		this._letter = letter;
		this._frequency = frequency;
	}

	get frequency(): number {
		return this._frequency;
	}

	get code(): number {
		return this._letter.charCodeAt(0);
	}
}
