import {binPowMod, gcd, rand} from './MathUtils';

/**
 * Primality test Miller-Rabin
 */
export function testMR(n: number, iteration: number) {
  if (n <= 1 || n === 4) {
    return false;
  }
  if (n <= 3) {
    return true;
  }

  let d = n - 1;
  while (d % 2 === 0) {
    d /= 2;
  }

  for (let i = 0; i < iteration; ++i) {
    if (!millerTest(d, n)) {
      return false;
    }
  }

  return true;
}


/**
 * Primality test Fermat
 */
export function testFermat(n: number, iteration: number) {
  if (n <= 1 || n === 4) {
    return false;
  }
  if (n <= 3) {
    return true;
  }

  while (iteration > 0) {
    const a = rand(2, n - 2);
    if (!isCoprime(n, a)) {
      return false;
    }

    if (binPowMod(a, n - 1, n) !== 1) {
      return false;
    }
    --iteration;
  }
  return true;
}
/**
 * Primality test Solovay-Strassen
 */
export function testSS(n: number, iteration: number): boolean {
  if (n < 2) {
    return false;
  }
  if (n !== 2 && n % 2 === 0) {
    return false;
  }

  for (let i = 0; i < iteration; i++) {
    const a = rand(1, n - 1);
    const jacobian = (n + calcJacobian(a, n)) % n;
    const mod = binPowMod(a, (n - 1) / 2, n);

    if (!jacobian || mod !== jacobian) {
      return false;
    }
  }
  return true;
}

export function isCoprime(a: number, b: number): boolean {
  return gcd(a, b) === 1;
}

function calcJacobian(a: number, n: number) {
  if (a === 0) { return 0; }
  let result = 1;
  if (a < 0) {
    a = -a;
    if (n % 4 === 3) {
      result = -result;
    }
  }
  if (a === 1) { return result; }
  while (a !== 0) {
    if (a < 0) {
      a = -a;
      if (n % 4 === 3) {
        result = -result;
      }
    }
    while (a % 2 === 0) {
      a /= 2;
      if (n % 8 === 3 || n % 8 === 5) {
        result = -result;
      }
    }

    const temp = a;
    a = n;
    n = temp;

    if (a % 4 === 3 && n % 4 === 3) {
      result = -result;
    }
    a %= n;

    if (a > (n / 2)) {
      a = a - n;
    }
  }

  if (n === 1) {
    return result;
  }
  return 0;
}

function millerTest(d: number, n: number) {
  const a = rand(2, n - 2);
  let x = binPowMod(a, d, n);
  if (x === 1 || x === n - 1) {
    return true;
  }

  while (d !== n - 1) {
    x = (x * x) % n;
    d *= 2;
    if (x === 1) {
      return false;
    }
    if (x === n - 1) {
      return true;
    }
  }
  return false;
}
