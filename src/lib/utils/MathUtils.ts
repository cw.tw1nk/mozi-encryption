/**
 * Stein's algorithm
 */
import {isCoprime} from './Prime';

export function gcd(a: number, b: number): number {
  if (a === 0) {
    return b;
  }
  if (b === 0) {
    return a;
  }

  let k;
  for (k = 0; ((a | b) & 1) === 0; ++k) {
    a >>= 1;
    b >>= 1;
  }

  while ((a & 1) === 0) {
    a >>= 1;
  }

  do {
    while ((b & 1) === 0) {
      b >>= 1;
    }
    if (a > b) {
      const temp = a;
      a = b;
      b = temp;
    }
    b -= a;
  } while (b !== 0);

  return a << k;
}

/**
 * O(log(n))
 * @param a 1/a
 * @param m module
 * @return 1/a mod m
 */
export function reverseMod(a: number, m: number): number {
  if (!isCoprime(a, m)) {
    return -1;
  }
  const phiM = phi(m);
  return binPowMod(a, phiM - 1, m);
}

/**
 * O(sqrt(n))
 * @param n arg euler function
 * @return value of euler function
 */
export function phi(n: number): number {
  let result = n;
  for (let i = 2; i * i <= n; i++) {
    if (n % i === 0) {
      while (n % i === 0) {
        n /= i;
      }
      result -= result / i;
    }
  }
  if (n > 1) {
    result -= result / n;
  }
  return result;
}

/**
 * O(log(n))
 */
export function binPowMod(a: number, b: number, m: number) {
  a %= m;
  let result = 1;
  while (b > 0) {
    if (b & 1) {
      result = result * a % m;
    }
    a = a * a % m;
    b >>= 1;
  }
  return result;
}

export function rand(min, max): number {
  return Math.ceil(Math.random() * (max - min) + min);
}
