import {IEncryption} from './IEncryption';
import * as _ from 'lodash';
export class Permutation implements IEncryption<number[]> {
  static chunkString(str, length) {
    return str.match(new RegExp('.{1,' + length + '}', 'g')).map(v => v);
  }

  decrypt(key: number[], text: string): string {
    if (key.length === 0) { return text; }
    const newKey = [];
    _.each(key, (value, index) => {
      newKey[value] = index;
    });
    return this.encrypt(newKey, text);
  }

  encrypt(key: number[], text: string): string {
    if (key.length === 0) { return text; }
    if (text.length === 0) { return ''; }
    const chunkedText: string[] = Permutation.chunkString(text, key.length);
    return _.join(_.map(chunkedText, (value) => {
      const result = [];
      _.each(value, (v, i) => {
        const index = key[i];
        result[index] = v;
      });
      return _.join(result, '');
    }), '');
  }

}
