export interface IEncryptionHack<KEY> {
	hack(encryptedText: string): KEY;
}
