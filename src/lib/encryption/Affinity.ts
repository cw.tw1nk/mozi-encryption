import {IEncryption} from './IEncryption';
import * as _ from 'lodash';
import * as Prime from '../utils/Prime';
import * as MathUtils from '../utils/MathUtils';
import {isCoprime} from '../utils/Prime';

export class Affinity implements IEncryption<{a: number, b: number}> {

  constructor(alphabet: string|Array<string>) {
    if (alphabet instanceof Array) {
      this.alphabet = alphabet.map(value => value.toLowerCase()).filter((value, index, array) => array.indexOf(value) === index);
    } else {
      this.alphabet = alphabet.toLowerCase().split('').filter((value, index, array) => array.indexOf(value) === index);
    }
  }
  private readonly alphabet: string[];

  decrypt(key: { a: number; b: number }, text: string): string {
    if (text.length === 0) { return ''; }
    if (this.alphabet.length === 0) { return text; }
    if (!Prime.isCoprime(key.a, this.alphabet.length)) { return text; }
    const a = MathUtils.reverseMod(key.a, this.alphabet.length);
    return _.map(text, (value) => {
      const x = _.indexOf(this.alphabet, value);
      if (x === -1) { return value; }
      let number = (a * (x - key.b)) % this.alphabet.length;
      if (number < 0) {
        number += this.alphabet.length;
      }
      return this.alphabet[number];
    }).join('');
  }

  encrypt(key: { a: number; b: number }, text: string): string {
    if (text.length === 0) { return ''; }
    if (this.alphabet.length === 0) { return text; }
    if (!isCoprime(key.a, this.alphabet.length)) { return text; }
    const unknowns = _.map(text, (value) => {
      const x = _.indexOf(this.alphabet, value);
      if (x === -1) { return value; }
      return this.alphabet[(key.a * x + key.b) % this.alphabet.length];
    });
    return unknowns.join('');
  }

}
