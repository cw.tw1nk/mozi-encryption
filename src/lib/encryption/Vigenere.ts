import {IEncryption} from './IEncryption';

export class Vigenere implements IEncryption<string> {
  private readonly alphabet: string[];

  constructor(alphabet: string|Array<string>) {
    if (alphabet instanceof Array) {
      this.alphabet = alphabet.map(value => value.toLowerCase()).filter((value, index, array) => array.indexOf(value) === index);
    } else {
      this.alphabet = alphabet.toLowerCase().split('').filter((value, index, array) => array.indexOf(value) === index);
    }
  }
  private getCode(str: string): number {
    for (let i = 0; i < this.alphabet.length; i++) {
      if (this.alphabet[i].toLowerCase() === str.toLowerCase()) {
        return i;
      }
    }
    return -1;
  }

  decrypt(key: string, encrypted_text: string): string {
    let result = '';
    const key_array = key.split('');
    const text_array = encrypted_text.split('');
    let index = 0;
    text_array.forEach(value => {
      const value_code = this.getCode(value);
      const key_code = this.getCode(key_array[index]);
      if (key_code !== -1 && value_code !== -1) {
        const char_code = (value_code + this.alphabet.length - key_code) % this.alphabet.length;
        result += this.alphabet[char_code];
        if (++index === key_array.length) {
          index = 0;
        }
      } else {
        result += value;
      }
    });
    return result;
  }

  encrypt(key: string, text: string): string {
    let result = '';
    key = key.toLowerCase();
    text = text.toLowerCase();
    const key_array = key.split('');
    const text_array = text.split('');
    let index = 0;
    text_array.forEach(value => {
      const value_code = this.getCode(value);
      const key_code = this.getCode(key_array[index]);
      const char_code = (value_code + key_code) % this.alphabet.length;
      if (key_code !== -1 && value_code !== -1 && char_code !== -1) {
        result += this.alphabet[char_code];
        if (++index === key_array.length) {
          index = 0;
        }
      } else {
        result += value;
      }
    });
    return result;
  }

}
