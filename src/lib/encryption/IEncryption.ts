
export interface IEncryption<T> {
	encrypt(key: T, text: string): string;
	decrypt(key: T, encrypted_text: string): string;
}
