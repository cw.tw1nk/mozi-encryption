import {IEncryption} from './IEncryption';
import {Unicode} from './Unicode';
import {IEncryptionHack} from './IEncryptionHack';
import {Frequency, FrequencyLetters} from '../utils/Frequency';

export class Caesar implements IEncryption<number>, IEncryptionHack<number> {
	encrypt(key: number, text: string): string {
		key %= Unicode.UTF_8_MAX;
		text = text.replace(/[\W]+/g, '');
		let value = '';
		for (let i = 0; i < text.length; i++) {
			value += String.fromCharCode((text.charCodeAt(i) + key) % Unicode.UTF_8_MAX);
		}

		return value;
	}
	decrypt(key: number, encrypted_text: string): string {
		let value = '';
		for (let i = 0; i < encrypted_text.length; i++) {
			value += String.fromCharCode(encrypted_text.charCodeAt(i) + Math.ceil(key / Unicode.UTF_8_MAX) * Unicode.UTF_8_MAX - key % Unicode.UTF_8_MAX);
		}

		return value;
	}

	hack(encryptedText: string): number {
		let frequency = Frequency.create(encryptedText);
		let englishFrequency = new Frequency(Frequency.frequencyByLanguage.english);
		let key = Math.abs(frequency.max().code - englishFrequency.max().code);
		return key;
	}
}
