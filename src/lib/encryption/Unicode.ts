export class Unicode {
	public static readonly UTF_8_MAX: number	=   0x010000;
	public static readonly UNICODE_MAX: number  =   0x10FFFF;
	public static readonly EN_Min: number 		=	0x41;
	public static readonly EN_Max: number 		= 	0x07a;
	public static readonly RU_Min: number 		= 	0x410;
	public static readonly RU_Max: number 		= 	0x44f;
	// private constructor() {}
	// public static fixedCharCodeAt(str: string, idx: number) {
	// 	idx = idx || 0;
	// 	var code = str.charCodeAt(idx);
	// 	var hi, low;
	// 	if (0xD800 <= code && code <= 0xDBFF) {
	// 		hi = code;
	// 		low = str.charCodeAt(idx + 1);
	// 		if (isNaN(low)) {
	// 			throw 'Nan';
	// 		}
	// 		return ((hi - 0xD800) * 0x400) + (low - 0xDC00) + this.UTF_8_MAX;
	// 	}
	// 	if (0xDC00 <= code && code <= 0xDFFF) {
	// 		return false;
	// 	}
	// 	return code;
	// }
}
