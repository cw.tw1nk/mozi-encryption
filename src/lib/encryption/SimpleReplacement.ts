import {IEncryption} from './IEncryption';

export class SimpleReplacement implements IEncryption<Map<string, string>> {
  decrypt(key: Map<string, string>, encrypted_text: string): string {
    return this.encrypt(SimpleReplacement.swap(key), encrypted_text);
  }

  encrypt(key: Map<string, string>, text: string): string {
    return this.replace(key, text);
  }

  private static swap(map: Map<string, string>): Map<string, string> {
    const temp = new Map<string, string>();
    map.forEach((value, key) => {
      temp.set(value, key);
    });
    return temp;
  }

  private replace(key: Map<string, string>, text: string): string {
    let result = '';
    for (let i = 0; i < text.length; i++) {
      if (key.has(text.charAt(i))) {
        result += key.get(text.charAt(i));
      } else {
        result += text.charAt(i);
      }
    }
    return result;
  }
}
