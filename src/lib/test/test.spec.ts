import {Caesar} from '../encryption/Caesar';
import {SimpleReplacement} from '../encryption/SimpleReplacement';
import {Frequency} from '../utils/Frequency';
import {Unicode} from '../encryption/Unicode';
import {Vigenere} from '../encryption/Vigenere';
import {Permutation} from '../encryption/Permutation';
import * as MathUtils from '../utils/MathUtils';
import * as Prime from '../utils/Prime';
import {Affinity} from '../encryption/Affinity';
const sum = (n1: number, n2: number) => n1 + n2;
describe('Lib', () => {
  it('true should be true', () => {
    expect(true).toBe(true);
  });

  it('2+2 should be 4', () => {
    expect(sum(2, 2)).toBe(4);
  });
  it('caesar', function() {
    expect(new Caesar().encrypt(1, 'a')).toBe('b');
    expect(new Caesar().decrypt(1, 'b')).toBe('a');
  });
  it('simple replacement', function() {
    const key = new Map<string, string>().set('a', 'b');
    expect(new SimpleReplacement().encrypt(key, 'a')).toBe('b');
    expect(new SimpleReplacement().encrypt(key, 'c')).toBe('c');
    expect(new SimpleReplacement().decrypt(key, new SimpleReplacement().encrypt(key, 'a'))).toBe('a');
  });
  it('vigenere', function() {
    {
      const vigenere = new Vigenere('abcdefgo');
      const text = 'addaddabab';
      const encrypted = vigenere.encrypt('dog', text);
      expect(encrypted).toBe('dcbdcbdage');
      expect(vigenere.decrypt('dog', encrypted)).toBe(text);
    }
    {
      const vigenere = new Vigenere('abcdefgo'.split(''));
      const text = 'addaddabab';
      const encrypted = vigenere.encrypt('dog', text);
      expect(encrypted).toBe('dcbdcbdage');
      expect(vigenere.decrypt('dog', encrypted)).toBe(text);
    }
    {
      const vigenere = new Vigenere('abcd'.split(''));
      const text = 'addaddabab';
      const encrypted = vigenere.encrypt('dog', text);
      expect(encrypted).toBe('dddaddabab');
      expect(vigenere.decrypt('dog', encrypted)).toBe(text);
    }
  });
  it('frequency', function() {
    expect(Frequency.create('asda').frequency.a).toBe(0.5);
    expect(Frequency.create('asda').frequency.s).toBe(0.25);
    expect(Frequency.create('asda').frequency.d).toBe(0.25);
    expect(Frequency.create('asda').frequency.e).toBe(undefined);
  });
  it('hack', function() {
    const key = 1;
    // tslint:disable-next-line:max-line-length
    const encryptedText = new Caesar().encrypt(key, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nibh tortor, aliquet eget velit vitae, porta dapibus massa. ' +
      'Sed sollicitudin mauris ut tempus condimentum. Maecenas a arcu nibh. Curabitur purus lacus, efficitur ut sollicitudin in, pharetra a augue. Suspendisse aliquet mattis fringilla. ' +
      'Pellentesque ullamcorper nunc eget libero porta, at porttitor nisi bibendum. Cras at tempus nunc. Duis dignissim lacinia consequat. Pellentesque placerat a elit non aliquet. ' +
      'Vestibulum dui libero, sollicitudin id pretium et, imperdiet pharetra leo. Ut gravida non erat at tincidunt. Sed quis justo in dui dignissim venenatis. ' +
      'Donec sodales sagittis tellus, in faucibus odio laoreet ac.\n' +
      '\n' +
      'Sed non dolor tincidunt quam tristique accumsan. Cras semper dapibus justo, sed viverra ipsum euismod sed. Curabitur efficitur, lectus in tempus malesuada, dui mi viverra felis, vitae ' +
      'pretium purus quam quis erat. Mauris in eros sollicitudin, congue nibh id, posuere erat. Vivamus ac felis quis nunc sollicitudin egestas. Cras vitae mauris a lacus pharetra viverra.' +
      ' Maecenas iaculis placerat quam, ac rhoncus erat luctus id. Suspendisse potenti. Aenean vel libero in eros convallis porttitor non in mauris. Vestibulum gravida nec lectus non semper. ' +
      'Aenean cursus lacus in porta cursus. Nullam facilisis hendrerit ex, at varius sem accumsan eget. Curabitur sed tortor sodales, hendrerit tortor sit amet, ultrices lectus. Donec eu orci ' +
      'in tellus molestie accumsan. Praesent rutrum tincidunt felis, sit amet vestibulum turpis.\n' +
      '\n' +
      'Curabitur facilisis dolor nec urna ullamcorper dictum. Pellentesque lobortis neque sed sapien vestibulum, eget lacinia enim pharetra. Sed viverra dolor justo, non elementum leo luctus ac. ' +
      'Curabitur tempor commodo nibh, quis blandit leo ultricies fermentum. Sed id interdum nisi, at laoreet risus. Nam ac eleifend mauris, eget condimentum enim. Nulla magna nisi, congue id ' +
      'nibh ut, blandit ultrices lacus. Nulla sit amet est nec erat tempor convallis. Etiam turpis enim, tristique sollicitudin congue a, rhoncus eu massa. Praesent et metus ut libero ' +
      'hendrerit sodales. Vestibulum ipsum sapien, gravida commodo risus eu, ullamcorper cursus felis. Fusce tincidunt arcu felis, et congue odio faucibus a. Vestibulum ante ipsum primis ' +
      'in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc commodo, arcu vel iaculis semper, urna orci tincidunt massa, vitae pulvinar magna sem nec ipsum.\n' +
      '\n' +
      'Proin fringilla fringilla lorem ut laoreet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vehicula elementum sem, vitae aliquet ' +
      'quam tincidunt at. Etiam imperdiet scelerisque arcu non blandit. Nam eget libero lobortis, hendrerit metus et, varius ex. Vestibulum ultricies velit sit amet interdum mattis. ' +
      'Proin et justo ultricies, dapibus diam eu, bibendum nibh. Integer egestas arcu non risus tempus posuere. Fusce accumsan est at est varius ullamcorper. Pellentesque quis congue ' +
      'velit. Aliquam faucibus enim velit, sed viverra sapien dictum id. Ut pretium justo sed ante volutpat, id congue urna sodales.');
    expect(new Caesar().hack(encryptedText)).toBe(key % Unicode.UTF_8_MAX);
  });
  it('permutation encrypt', function() {
    const permutation = new Permutation();
    const encryptedText = permutation.encrypt([3, 1, 4, 2], 'matfakuniversity');
    expect(encryptedText).toBe('afmtknauvrieiyst');
    expect(permutation.encrypt([], 'matfakuniversity')).toBe('matfakuniversity');
    expect(permutation.encrypt([], '')).toBe('');
    expect(permutation.encrypt([1, 2, 3, 4], '')).toBe('');
    /*
     * afmt knau vrie iyst
     * [3, 1, 4, 2]
     * [1, 3, 0, 2]
     */
    expect(permutation.decrypt([2, 0, 3, 1], encryptedText)).toBe('matfakuniversity');
    expect(permutation.decrypt([], encryptedText)).toBe(encryptedText);
  });
  it('chunking string', function() {
    expect(Permutation.chunkString('texttexts', 4)).toEqual(['text', 'text', 's']);
  });
  it('affinity', function() {
    // const check = (message, crypted_message, x, y) => {
    //   const affinity = new Affinity('abcdefghijklmnopqrstuvwxyz');
    //   const s = affinity.encrypt({a: x, b: y}, message);
    //   expect(s).toBe(crypted_message);
    //   expect(affinity.decrypt({a: x, b: y}, s)).toBe(message);
    // };

    // check('test', 'nien', 718051, 128);
    // check('test', 'nien', 718051, 128);
    // check('test', 'test', 124, 128);
    const affinity = new Affinity('abcdefghijklmnopqrstuvwxyz');
    const s = affinity.encrypt({a: 72174371, b: 2}, 'tomorrow');
    console.log(s);
    console.log(affinity.decrypt({a: 72174371, b: 2}, s));
  });


  it('euler function', function() {
    console.log(MathUtils.phi(98));
    expect(MathUtils.phi(10)).toBe(4);
    expect(MathUtils.phi(1)).toBe(1);
    expect(MathUtils.phi(2)).toBe(1);
    expect(MathUtils.phi(3)).toBe(2);
    expect(MathUtils.phi(4)).toBe(2);
    expect(MathUtils.phi(3)).toBe(2);
    expect(MathUtils.phi(98)).toBe(42);
  });
  it('should be calc gcd', function() {
    expect(MathUtils.gcd(2, 2)).toBe(2);
    expect(MathUtils.gcd(2, 5)).toBe(1);
    expect(MathUtils.gcd(11, 30)).toBe(1);
    expect(MathUtils.gcd(7, 30)).toBe(1);
    expect(MathUtils.gcd(3, 2)).toBe(1);
    expect(MathUtils.gcd(20, 34)).toBe(2);
    expect(MathUtils.gcd(30, 35)).toBe(5);
    expect(MathUtils.gcd(128, 140)).toBe(4);
    expect(MathUtils.gcd(99, 27)).toBe(9);
  });
  it('should be coprime', function() {
    expect(Prime.isCoprime(11, 30)).toBeTrue();
    expect(Prime.isCoprime(17, 2)).toBeTrue();
    expect(Prime.isCoprime(17, 3)).toBeTrue();
    expect(Prime.isCoprime(17, 4)).toBeTrue();
    expect(Prime.isCoprime(17, 5)).toBeTrue();
    expect(Prime.isCoprime(17, 6)).toBeTrue();
    expect(Prime.isCoprime(17, 7)).toBeTrue();
    expect(Prime.isCoprime(17, 8)).toBeTrue();
    expect(Prime.isCoprime(229, 223)).toBeTrue();
    expect(Prime.isCoprime(229, 183)).toBeTrue();
    expect(Prime.isCoprime(175, 167)).toBeTrue();
  });
  it('should be not coprime', function() {
    expect(Prime.isCoprime(2, 4)).toBeFalse();
    expect(Prime.isCoprime(4, 8)).toBeFalse();
    expect(Prime.isCoprime(4, 10)).toBeFalse();
    expect(Prime.isCoprime(3, 9)).toBeFalse();
    expect(Prime.isCoprime(175, 168)).toBeFalse();
    expect(Prime.isCoprime(180, 189)).toBeFalse();
    expect(Prime.isCoprime(189, 195)).toBeFalse();
    expect(Prime.isCoprime(206, 212)).toBeFalse();
    expect(Prime.isCoprime(212, 228)).toBeFalse();
  });
  it('should be calc reverse module', function() {
    expect(MathUtils.reverseMod(8, 2)).toBe(-1);
    expect(MathUtils.reverseMod(8, 77)).toBe(29);
    expect(MathUtils.reverseMod(123, 4567)).toBe(854);
    expect(MathUtils.reverseMod(129, 4567)).toBe(2195);
    expect(MathUtils.reverseMod(23, 4567)).toBe(1390);
  });

  it('should be test fermat return true', function() {
    const k = 10;
    expect(Prime.testFermat(11, k)).toBeTrue();
    expect(Prime.testFermat(13, k)).toBeTrue();
    expect(Prime.testFermat(17, k)).toBeTrue();
    expect(Prime.testFermat(19, k)).toBeTrue();
    expect(Prime.testFermat(23, k)).toBeTrue();
    expect(Prime.testFermat(29, k)).toBeTrue();
    expect(Prime.testFermat(31, k)).toBeTrue();
    expect(Prime.testFermat(37, k)).toBeTrue();
    expect(Prime.testFermat(41, k)).toBeTrue();
    expect(Prime.testFermat(43, k)).toBeTrue();
    expect(Prime.testFermat(47, k)).toBeTrue();
    expect(Prime.testFermat(53, k)).toBeTrue();
    expect(Prime.testFermat(59, k)).toBeTrue();
    expect(Prime.testFermat(10007, k)).toBeTrue();
    expect(Prime.testFermat(10159, k)).toBeTrue();
    expect(Prime.testFermat(1000003, k)).toBeTrue();
    expect(Prime.testFermat(1000033, k)).toBeTrue();
    expect(Prime.testFermat(1000037, k)).toBeTrue();
    expect(Prime.testFermat(1000039, k)).toBeTrue();
    expect(Prime.testFermat(1000249, k)).toBeTrue();
  });
  it('should be test fermat return false', function() {
    const k = 10;
    expect(Prime.testFermat(11 * 2, k)).toBeFalse();
    expect(Prime.testFermat(13 * 2, k)).toBeFalse();
    expect(Prime.testFermat(17 * 2, k)).toBeFalse();
    expect(Prime.testFermat(19 * 2, k)).toBeFalse();
    expect(Prime.testFermat(23 * 2, k)).toBeFalse();
    expect(Prime.testFermat(29 * 2, k)).toBeFalse();
    expect(Prime.testFermat(31 * 2, k)).toBeFalse();
    expect(Prime.testFermat(37 * 2, k)).toBeFalse();
    expect(Prime.testFermat(41 * 2, k)).toBeFalse();
    expect(Prime.testFermat(43 * 2, k)).toBeFalse();
    expect(Prime.testFermat(47 * 2, k)).toBeFalse();
    expect(Prime.testFermat(53 * 2, k)).toBeFalse();
    expect(Prime.testFermat(59 * 2, k)).toBeFalse();
    expect(Prime.testFermat(10007 * 2, k)).toBeFalse();
    expect(Prime.testFermat(10159 * 2, k)).toBeFalse();
    expect(Prime.testFermat(1000003 * 2, k)).toBeFalse();
    expect(Prime.testFermat(1000033 * 2, k)).toBeFalse();
    expect(Prime.testFermat(1000037 * 2, k)).toBeFalse();
    expect(Prime.testFermat(1000039 * 2, k)).toBeFalse();
    expect(Prime.testFermat(1000249 * 2, k)).toBeFalse();
  });
  it('should be test MR return true', function() {
    const k = 10;
    expect(Prime.testMR(11, k)).toBeTrue();
    expect(Prime.testMR(13, k)).toBeTrue();
    expect(Prime.testMR(17, k)).toBeTrue();
    expect(Prime.testMR(19, k)).toBeTrue();
    expect(Prime.testMR(23, k)).toBeTrue();
    expect(Prime.testMR(29, k)).toBeTrue();
    expect(Prime.testMR(31, k)).toBeTrue();
    expect(Prime.testMR(37, k)).toBeTrue();
    expect(Prime.testMR(41, k)).toBeTrue();
    expect(Prime.testMR(43, k)).toBeTrue();
    expect(Prime.testMR(47, k)).toBeTrue();
    expect(Prime.testMR(53, k)).toBeTrue();
    expect(Prime.testMR(59, k)).toBeTrue();
    expect(Prime.testMR(10007, k)).toBeTrue();
    expect(Prime.testMR(10159, k)).toBeTrue();
    expect(Prime.testMR(1000003, k)).toBeTrue();
    expect(Prime.testMR(1000033, k)).toBeTrue();
    expect(Prime.testMR(1000037, k)).toBeTrue();
    expect(Prime.testMR(1000039, k)).toBeTrue();
    expect(Prime.testMR(1000249, k)).toBeTrue();
    expect(Prime.testMR(11000273, k)).toBeTrue();
  });
  it('should be test MR return false', function() {
    const k = 10;
    expect(Prime.testMR(11 * 2, k)).toBeFalse();
    expect(Prime.testMR(13 * 2, k)).toBeFalse();
    expect(Prime.testMR(17 * 2, k)).toBeFalse();
    expect(Prime.testMR(19 * 2, k)).toBeFalse();
    expect(Prime.testMR(23 * 2, k)).toBeFalse();
    expect(Prime.testMR(29 * 2, k)).toBeFalse();
    expect(Prime.testMR(31 * 2, k)).toBeFalse();
    expect(Prime.testMR(37 * 2, k)).toBeFalse();
    expect(Prime.testMR(41 * 2, k)).toBeFalse();
    expect(Prime.testMR(43 * 2, k)).toBeFalse();
    expect(Prime.testMR(47 * 2, k)).toBeFalse();
    expect(Prime.testMR(53 * 2, k)).toBeFalse();
    expect(Prime.testMR(59 * 2, k)).toBeFalse();
    expect(Prime.testMR(10007 * 2, k)).toBeFalse();
    expect(Prime.testMR(10159 * 2, k)).toBeFalse();
    expect(Prime.testMR(1000003 * 2, k)).toBeFalse();
    expect(Prime.testMR(1000033 * 2, k)).toBeFalse();
    expect(Prime.testMR(1000037 * 2, k)).toBeFalse();
    expect(Prime.testMR(1000039 * 2, k)).toBeFalse();
    expect(Prime.testMR(1000249 * 2, k)).toBeFalse();
  });
  it('should be test SS return true', function() {
    const k = 10;
    expect(Prime.testSS(11, k)).toBeTrue();
    expect(Prime.testSS(13, k)).toBeTrue();
    expect(Prime.testSS(17, k)).toBeTrue();
    expect(Prime.testSS(19, k)).toBeTrue();
    expect(Prime.testSS(23, k)).toBeTrue();
    expect(Prime.testSS(29, k)).toBeTrue();
    expect(Prime.testSS(31, k)).toBeTrue();
    expect(Prime.testSS(37, k)).toBeTrue();
    expect(Prime.testSS(41, k)).toBeTrue();
    expect(Prime.testSS(43, k)).toBeTrue();
    expect(Prime.testSS(47, k)).toBeTrue();
    expect(Prime.testSS(53, k)).toBeTrue();
    expect(Prime.testSS(59, k)).toBeTrue();
    expect(Prime.testSS(10007, k)).toBeTrue();
    expect(Prime.testSS(10159, k)).toBeTrue();
    expect(Prime.testSS(1000003, k)).toBeTrue();
    expect(Prime.testSS(1000033, k)).toBeTrue();
    expect(Prime.testSS(1000037, k)).toBeTrue();
    expect(Prime.testSS(1000039, k)).toBeTrue();
    expect(Prime.testSS(1000249, k)).toBeTrue();
    expect(Prime.testSS(11000273, k)).toBeTrue();
  });
  it('should be test SS return false', function() {
    const k = 10;
    expect(Prime.testSS(11 * 2, k)).toBeFalse();
    expect(Prime.testSS(13 * 2, k)).toBeFalse();
    expect(Prime.testSS(17 * 2, k)).toBeFalse();
    expect(Prime.testSS(19 * 2, k)).toBeFalse();
    expect(Prime.testSS(23 * 2, k)).toBeFalse();
    expect(Prime.testSS(29 * 2, k)).toBeFalse();
    expect(Prime.testSS(31 * 2, k)).toBeFalse();
    expect(Prime.testSS(37 * 2, k)).toBeFalse();
    expect(Prime.testSS(41 * 2, k)).toBeFalse();
    expect(Prime.testSS(43 * 2, k)).toBeFalse();
    expect(Prime.testSS(47 * 2, k)).toBeFalse();
    expect(Prime.testSS(53 * 2, k)).toBeFalse();
    expect(Prime.testSS(59 * 2, k)).toBeFalse();
    expect(Prime.testSS(10007 * 2, k)).toBeFalse();
    expect(Prime.testSS(10159 * 2, k)).toBeFalse();
    expect(Prime.testSS(1000003 * 2, k)).toBeFalse();
    expect(Prime.testSS(1000033 * 2, k)).toBeFalse();
    expect(Prime.testSS(1000037 * 2, k)).toBeFalse();
    expect(Prime.testSS(1000039 * 2, k)).toBeFalse();
    expect(Prime.testSS(1000249 * 2, k)).toBeFalse();
  });
});
