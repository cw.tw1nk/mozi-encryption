import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { RouterModule, Routes } from '@angular/router';
import { CaesarEncComponent } from './caesar/caesar-enc.component';
import { SimpleReplacementEncComponent } from './simple-replacement/simple-replacement-enc.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { HTMLEscapeUnescapeModule } from 'html-escape-unescape';
import { VigenereEncComponent } from './vigenere-cipher/vigenere-enc.component';
import { PermutationEncComponent } from './permutation-enc/permutation-enc.component';
import { AffinityEncComponent } from './affinity-enc/affinity-enc.component';
import { AffinityHighEncComponent } from './affinity-high-enc/affinity-high-enc.component';

const appRoutes: Routes = [
  {
    path: 'ce',
    component: CaesarEncComponent
  },
  {
    path: 'sre',
    component: SimpleReplacementEncComponent,
    // data: { title: 'Heroes List' }
  },
  {
    path: 've',
    component: VigenereEncComponent,
  },
  {
    path: 'pe',
    component: PermutationEncComponent,
  },
  {
    path: 'ae',
    component: AffinityEncComponent,
  },
  {
    path: 'ahe',
    component: AffinityHighEncComponent,
  },
  { path: '',
    redirectTo: '/ce',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CaesarEncComponent,
    SimpleReplacementEncComponent,
    PageNotFoundComponent,
    HeaderComponent,
    VigenereEncComponent,
    PermutationEncComponent,
    AffinityEncComponent,
    AffinityHighEncComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    HTMLEscapeUnescapeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
