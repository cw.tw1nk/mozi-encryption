import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {Affinity} from '../../lib/encryption/Affinity';
import * as Prime from '../../lib/utils/Prime';
import fortuna from 'javascript-fortuna';
import jsspg from 'javascript-strong-password-generator';
import sha512 from 'js-sha512';
function entropyFunction() {
  return jsspg.entropyVal;
}
@Component({
  selector: 'app-affinity-enc',
  templateUrl: './affinity-enc.component.html',
  styleUrls: ['./affinity-enc.component.css']
})
export class AffinityEncComponent implements OnInit {

  myForm: FormGroup;
  private _alphabet: Set<string> = new Set('abcdefghijklmnopqrstuvwxyz~`1234567890-=!@#$%^&*()_+[];\',./{}:"<>?№ёЁйцукенгшщзхъфывапролджэячсмитьбю.ХЪЖЭБЮ,'.split(''));
  public _ = _;
  private fortuna_init = true;

  constructor() {
    this.myForm = new FormGroup({
      text: new FormControl('', []),
      key_a: new FormControl('31', [Validators.required]),
      key_b: new FormControl('2', [Validators.required]),
      char: new FormControl('a', [Validators.required, Validators.pattern(/^[\s\S]+$/i)]),
    });
    fortuna.init();
    document.onmousemove = (ev) => {
      // @ts-ignore
      jsspg.entropyVal = sha512(`${JSON.stringify(ev)}:${JSON.stringify(ev.x)}:${JSON.stringify(ev.y)}:${JSON.stringify(ev.x * ev.y)}`);
      if (this.fortuna_init) {
        fortuna.init({ timeBasedEntropy: true, accumulateTimeout: 1000, entropyFxn: entropyFunction });
        this.fortuna_init = false;
      }
    };
  }

  ngOnInit(): void {

  }

  get alphabet(): Set<string> {
    return this._alphabet;
  }

  get key_a() { return this.myForm.get('key_a'); }
  get key_b() { return this.myForm.get('key_b'); }
  get char() { return this.myForm.get('char'); }

  encrypt() {
    if (!this.isCoprime()) {
      this.myForm.setErrors({coprime: 'Value is not coprime with ' + this.alphabet.size});
      return;
    }
    this.myForm.setErrors({coprime: null});
    if (this.alphabet.size > 0) {
      const alphabet: string[] = [];
      this.alphabet.forEach(value => {
        alphabet.push(value);
      });
      this.myForm.get('text').setValue((new Affinity(alphabet)).encrypt({a: parseInt(this.key_a.value, 0), b: parseInt(this.key_b.value, 0)}, this.myForm.get('text').value));
    }
  }

  decrypt() {
    if (!this.isCoprime()) {
      this.myForm.setErrors({coprime: 'Value is not coprime with ' + this.alphabet.size});
      return;
    }
    this.myForm.setErrors({coprime: null});
    if (this.alphabet.size > 0) {
      const alphabet: string[] = [];
      this.alphabet.forEach(value => {
        alphabet.push(value);
      });
      this.myForm.get('text').setValue((new Affinity(alphabet)).decrypt({a: parseInt(this.key_a.value, 0), b: parseInt(this.key_b.value, 0)}, this.myForm.get('text').value));
    }
  }

  generateRandomKey() {
    this.myForm.setErrors({coprime: null});
    const min = 1024;
    const max = 1000000000;
    while (true) {
      const randomInt = Math.ceil(fortuna.random() * (max - min)) + min;
      if (Prime.testSS(randomInt, 25)) {
        this.key_a.setValue(randomInt);
        break;
      }
    }
  }

  isCoprime() {
    return Prime.isCoprime(parseInt(this.key_a.value as string, 0), this._alphabet.size);
  }

  add(value: string) {
    value.split('').forEach( val => {
      if (!this.alphabet.has(val)) {
        this.alphabet.add(val);
      }
    });
  }
}
