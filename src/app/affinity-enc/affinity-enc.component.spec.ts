import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffinityEncComponent } from './affinity-enc.component';

describe('AffinityEncComponent', () => {
  let component: AffinityEncComponent;
  let fixture: ComponentFixture<AffinityEncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffinityEncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffinityEncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
