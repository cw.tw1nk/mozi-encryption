import {Component, OnInit} from '@angular/core';
import {FormGroup, FormArray, FormControl, Validators} from '@angular/forms';
import {SimpleReplacement} from '../../lib/encryption/SimpleReplacement';

@Component({
  selector: 'app-simple-replacement',
  templateUrl: './simple-replacement-enc.component.html',
  styleUrls: ['./simple-replacement-enc.component.css']
})
export class SimpleReplacementEncComponent implements OnInit {
  myForm: FormGroup;
  alphabet: Map<string, string> = new Map();
  json = {
    stringify(val) {
      const call = JSON.stringify(val) as string;
      if (call.length <= 2) {
        return call;
      }
      return call.substr(1, call.length - 2).replace(' ', '\\s');
    },
    parse: JSON.parse
  };
  private simpleReplacement = new SimpleReplacement();

  constructor() {
    this.myForm = new FormGroup({
      text: new FormControl('', []),
      key: new FormControl('a', [Validators.required, Validators.pattern(/^[\s\S]$/i)]),
      value: new FormControl('b', [Validators.required, Validators.pattern(/^[\s\S]$/i)]),
    });
    this.alphabet.set('a', 'b');
    this.alphabet.set('\t', 'x');
    this.alphabet.set(' ', '\t');
    this.alphabet.set('\n', '.');
  }

  get key() { return this.myForm.get('key'); }

  get value() { return this.myForm.get('value'); }

  ngOnInit(): void {
  }



  encrypt() {
    if (this.alphabet.size > 0) {
      this.myForm.get('text').setValue(this.simpleReplacement.encrypt(this.alphabet, this.myForm.get('text').value));
    }
  }

  decrypt() {
    if (this.alphabet.size > 0) {
      this.myForm.get('text').setValue(this.simpleReplacement.decrypt(this.alphabet, this.myForm.get('text').value));
    }
  }

  addReplace() {
    console.log(this.myForm);
    if (this.myForm.valid) {
      this.alphabet.set(this.key.value, this.value.value);
    }
  }

  check(value: any) {
    for (const val of this.alphabet.values()) {
      if (val === value.value) {
        return false;
      }
    }
    return true;
  }
}
