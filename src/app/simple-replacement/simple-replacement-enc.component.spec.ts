import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleReplacementEncComponent } from './simple-replacement-enc.component';

describe('SimpleReplacementComponent', () => {
  let component: SimpleReplacementEncComponent;
  let fixture: ComponentFixture<SimpleReplacementEncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleReplacementEncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleReplacementEncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
