import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  template: `
    <app-header></app-header>
    <div class="uk-container uk-container-large">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
  title = 'mozi';
}
