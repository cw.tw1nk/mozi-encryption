import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Permutation} from '../../lib/encryption/Permutation';
import * as _ from 'lodash';
@Component({
  selector: 'app-permutation-cipher',
  templateUrl: './permutation-enc.component.html',
  styleUrls: ['./permutation-enc.component.css']
})

export class PermutationEncComponent implements OnInit {
  form: FormGroup;
  key: number[] = _.shuffle<number>([0, 1, 2, 3, 4, 5, 6, 7, 8]);
  _ = _;
  constructor() {
    this.form = new FormGroup({
      text: new FormControl('', []),
      keyChar: new FormControl('1', [Validators.required, Validators.pattern(/[0-9]+/)])
    });
  }

  ngOnInit(): void {
  }

  get keyChar() { return this.form.get('keyChar'); }
  get text() { return this.form.get('text'); }

  encrypt() {
    this.text.setValue(new Permutation().encrypt(this.key, this.text.value));
  }

  decrypt() {
    this.text.setValue(new Permutation().decrypt(this.key, this.text.value));  }

  remove(item: number) {
    this.key = _.remove(this.key, (value) => value !== item);
  }

  has(value: string) {
    if (value.length <= 0) { return false; }
    return _.includes<number>(this.key, _.parseInt(value));
  }
}
