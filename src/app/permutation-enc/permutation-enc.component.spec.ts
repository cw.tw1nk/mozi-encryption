import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermutationEncComponent } from './permutation-enc.component';

describe('PermutationCipherComponent', () => {
  let component: PermutationEncComponent;
  let fixture: ComponentFixture<PermutationEncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermutationEncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermutationEncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
