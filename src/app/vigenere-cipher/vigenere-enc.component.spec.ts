import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VigenereEncComponent } from './vigenere-enc.component';

describe('VigenereCipherComponent', () => {
  let component: VigenereEncComponent;
  let fixture: ComponentFixture<VigenereEncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VigenereEncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VigenereEncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
