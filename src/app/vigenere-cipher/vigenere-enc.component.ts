import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IEncryption} from '../../lib/encryption/IEncryption';
import {Vigenere} from '../../lib/encryption/Vigenere';
import * as _ from 'lodash';

@Component({
  selector: 'app-vigenere-cipher',
  templateUrl: './vigenere-enc.component.html',
  styleUrls: ['./vigenere-enc.component.css']
})
export class VigenereEncComponent implements OnInit {
  myForm: FormGroup;
  private _alphabet: Set<string> = new Set('abcdefghijklmnopqrstuvwxyz~`1234567890-=!@#$%^&*()_+[];\',./{}:"<>?№ёЁйцукенгшщзхъфывапролджэячсмитьбю.ХЪЖЭБЮ,'.split(''));
  public _ = _;

  constructor() {
    this.myForm = new FormGroup({
      text: new FormControl('', []),
      key: new FormControl('a', [Validators.required]),
      char: new FormControl('a', [Validators.required, Validators.pattern(/^[\s\S]$/i)]),
    });
  }

  ngOnInit(): void {
  }


  get alphabet(): Set<string> {
    return this._alphabet;
  }

  get key() { return this.myForm.get('key'); }
  get char() { return this.myForm.get('char'); }

  encrypt() {
    if (this.alphabet.size > 0) {
      const alphabet: string[] = [];
      this.alphabet.forEach(value => {
        alphabet.push(value);
      });
      this.myForm.get('text').setValue((new Vigenere(alphabet)).encrypt(this.key.value, this.myForm.get('text').value));
    }
  }

  decrypt() {
    if (this.alphabet.size > 0) {
      const alphabet: string[] = [];
      this.alphabet.forEach(value => {
        alphabet.push(value);
      });
      this.myForm.get('text').setValue((new Vigenere(alphabet)).decrypt(this.key.value, this.myForm.get('text').value));
    }
  }
}
