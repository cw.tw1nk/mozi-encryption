import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaesarEncComponent } from './caesar-enc.component';

describe('CaesarComponent', () => {
  let component: CaesarEncComponent;
  let fixture: ComponentFixture<CaesarEncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaesarEncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaesarEncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
