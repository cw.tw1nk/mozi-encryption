import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Caesar} from '../../lib/encryption/Caesar';

@Component({
  selector: 'app-caesar',
  templateUrl: './caesar-enc.component.html',
  styleUrls: ['./caesar-enc.component.css']
})
export class CaesarEncComponent {
  form = new FormGroup({});
  private readonly caesar = new Caesar();
  model = {
    key: 0,
    text: ''
  };
  constructor() { }
  encrypt() {
    console.log(this.model);
    this.model.text = this.caesar.encrypt(this.model.key, this.model.text);
  }
  decrypt() {
    console.log(this.model);
    this.model.text = this.caesar.encrypt(this.model.key, this.model.text);
  }
}
