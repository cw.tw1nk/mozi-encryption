import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffinityHighEncComponent } from './affinity-high-enc.component';

describe('AffinityHighEncComponent', () => {
  let component: AffinityHighEncComponent;
  let fixture: ComponentFixture<AffinityHighEncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffinityHighEncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffinityHighEncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
