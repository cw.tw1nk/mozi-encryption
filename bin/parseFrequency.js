const Promise = require("bluebird");
const fs = require("fs");
const lineReader = require('line-reader');
let lines = [];
let eachLine = Promise.promisify(lineReader.eachLine);
eachLine('frequency.txt', function(line) {
	let s = line.replace(/[()%~]+/g, ' ').replace(/[\s]+/g, ' ');
	lines.push(s.split(/[\s]/).filter(value => (value.length > 0 && !/^\s+$/.test(value))));
}).then(function() {
	if (lines.length > 0) {
		let obj = {};
		for (let i = 1; i < lines[0].length; i++) {
			obj[lines[0][i].toLowerCase()] = getLang(lines[0][i]);
		}
		console.log(obj);
		fs.writeFileSync("frequency.json", JSON.stringify(obj));
		console.log('done');
	} else {
		console.log('error');
	}
}).catch(function(err) {
	console.error(err);
});

function getLang(str) {
	let value = {};
	if (lines.length > 0) {
		let index = lines[0].indexOf(str);
		if (index >= 0) {
			for (let i = 1; i < lines.length; i++) {
				let line = lines[i];
				if (line.length > 1 && line.length > index) {
					value[line[0]] = parseFloat(line[index])/100;
				}
			}
		}
	}
	return value;
}
